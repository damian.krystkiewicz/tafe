BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "app_user"
(
    "id"           integer      NOT NULL PRIMARY KEY AUTOINCREMENT,
    "fist_name"    varchar(63)  NOT NULL,
    "middle_names" varchar(127) NULL,
    "last_name"    varchar(63)  NOT NULL,
    "username"     varchar(63)  NOT NULL,
    "password"     varchar(63)  NOT NULL
);
INSERT INTO app_user
VALUES (1, 'OzzyName0', 'OzzyMidName0', 'OzzyLastName0', 'OzzyUsername0', 'dfkb3aanbi4gv0');
INSERT INTO app_user
VALUES (2, 'OzzyName1', 'OzzyMidName1', 'OzzyLastName1', 'OzzyUsername1', 'dfkb3aanbi4gv1');
INSERT INTO app_user
VALUES (3, 'OzzyName2', 'OzzyMidName2', 'OzzyLastName2', 'OzzyUsername2', 'dfkb3aanbi4gv2');
INSERT INTO app_user
VALUES (4, 'OzzyName3', 'OzzyMidName3', 'OzzyLastName3', 'OzzyUsername3', 'dfkb3aanbi4gv3');
INSERT INTO app_user
VALUES (5, 'OzzyName4', 'OzzyMidName4', 'OzzyLastName4', 'OzzyUsername4', 'dfkb3aanbi4gv4');
INSERT INTO app_user
VALUES (6, 'OzzyEmployer0', 'OzzyEmployerMidName0', 'OzzyEmployerLastName0', 'OzzyEmployer0', 'dfkb3aanbi4gv0');
INSERT INTO app_user
VALUES (7, 'OzzyEmployer1', 'OzzyEmployerMidName1', 'OzzyEmployerLastName1', 'OzzyEmployer1', 'dfkb3aanbi4gv1');
INSERT INTO app_user
VALUES (8, 'OzzyEmployer2', 'OzzyEmployerMidName2', 'OzzyEmployerLastName2', 'OzzyEmployer2', 'dfkb3aanbi4gv2');
INSERT INTO app_user
VALUES (9, 'OzzyEmployer3', 'OzzyEmployerMidName3', 'OzzyEmployerLastName3', 'OzzyEmployer3', 'dfkb3aanbi4gv3');
INSERT INTO app_user
VALUES (10, 'OzzyEmployer4', 'OzzyEmployerMidName4', 'OzzyEmployerLastName4', 'OzzyEmployer4', 'dfkb3aanbi4gv4');
INSERT INTO app_user
VALUES (11, 'OzzyEmployer5', 'OzzyEmployerMidName5', 'OzzyEmployerLastName5', 'OzzyEmployer5', 'dfkb3aanbi4gv5');
CREATE TABLE IF NOT EXISTS "app_position"
(
    "id"                  integer      NOT NULL PRIMARY KEY AUTOINCREMENT,
    "title"               varchar(63)  NOT NULL,
    "description"         varchar(255) NOT NULL,
    "responsibilities"    varchar(511) NOT NULL,
    "skills"              varchar(511) NOT NULL,
    "requirements"        varchar(511) NOT NULL,
    "years_of_experience" varchar(511) NOT NULL,
    "salary_range"        varchar(31)  NULL,
    "location"            varchar(63)  NOT NULL,
    "contract_type"       varchar(63)  NULL,
    "employer_id"         integer      NOT NULL REFERENCES "app_employer" ("id") DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO app_position
VALUES (1, 'Position 0', 'Job position 0 descriptipn', 'Job position 0 responsibilities', 'Job position 0 skills',
        'Job position 0 requirements', '0', '0.0-0.0', 'Brisbane', 'Permanent', 1);
INSERT INTO app_position
VALUES (2, 'Position 1', 'Job position 1 descriptipn', 'Job position 1 responsibilities', 'Job position 1 skills',
        'Job position 1 requirements', '1', '1000.0-2000.0', 'Brisbane', 'Permanent', 1);
INSERT INTO app_position
VALUES (3, 'Position 2', 'Job position 2 descriptipn', 'Job position 2 responsibilities', 'Job position 2 skills',
        'Job position 2 requirements', '2', '2000.0-4000.0', 'Brisbane', 'Permanent', 1);
INSERT INTO app_position
VALUES (4, 'Position 3', 'Job position 3 descriptipn', 'Job position 3 responsibilities', 'Job position 3 skills',
        'Job position 3 requirements', '3', '3000.0-6000.0', 'Brisbane', 'Permanent', 1);
INSERT INTO app_position
VALUES (5, 'Position 0', 'Job position 0 descriptipn', 'Job position 0 responsibilities', 'Job position 0 skills',
        'Job position 0 requirements', '0', '0.0-0.0', 'Brisbane', 'Permanent', 2);
INSERT INTO app_position
VALUES (6, 'Position 1', 'Job position 1 descriptipn', 'Job position 1 responsibilities', 'Job position 1 skills',
        'Job position 1 requirements', '1', '1000.0-2000.0', 'Brisbane', 'Permanent', 2);
INSERT INTO app_position
VALUES (7, 'Position 2', 'Job position 2 descriptipn', 'Job position 2 responsibilities', 'Job position 2 skills',
        'Job position 2 requirements', '2', '2000.0-4000.0', 'Brisbane', 'Permanent', 2);
INSERT INTO app_position
VALUES (8, 'Position 3', 'Job position 3 descriptipn', 'Job position 3 responsibilities', 'Job position 3 skills',
        'Job position 3 requirements', '3', '3000.0-6000.0', 'Brisbane', 'Permanent', 2);
INSERT INTO app_position
VALUES (9, 'Position 0', 'Job position 0 descriptipn', 'Job position 0 responsibilities', 'Job position 0 skills',
        'Job position 0 requirements', '0', '0.0-0.0', 'Brisbane', 'Permanent', 3);
INSERT INTO app_position
VALUES (10, 'Position 1', 'Job position 1 descriptipn', 'Job position 1 responsibilities', 'Job position 1 skills',
        'Job position 1 requirements', '1', '1000.0-2000.0', 'Brisbane', 'Permanent', 3);
INSERT INTO app_position
VALUES (11, 'Position 2', 'Job position 2 descriptipn', 'Job position 2 responsibilities', 'Job position 2 skills',
        'Job position 2 requirements', '2', '2000.0-4000.0', 'Brisbane', 'Permanent', 3);
INSERT INTO app_position
VALUES (12, 'Position 3', 'Job position 3 descriptipn', 'Job position 3 responsibilities', 'Job position 3 skills',
        'Job position 3 requirements', '3', '3000.0-6000.0', 'Brisbane', 'Permanent', 3);
INSERT INTO app_position
VALUES (13, 'Position 0', 'Job position 0 descriptipn', 'Job position 0 responsibilities', 'Job position 0 skills',
        'Job position 0 requirements', '0', '0.0-0.0', 'Brisbane', 'Permanent', 4);
INSERT INTO app_position
VALUES (14, 'Position 1', 'Job position 1 descriptipn', 'Job position 1 responsibilities', 'Job position 1 skills',
        'Job position 1 requirements', '1', '1000.0-2000.0', 'Brisbane', 'Permanent', 4);
INSERT INTO app_position
VALUES (15, 'Position 2', 'Job position 2 descriptipn', 'Job position 2 responsibilities', 'Job position 2 skills',
        'Job position 2 requirements', '2', '2000.0-4000.0', 'Brisbane', 'Permanent', 4);
INSERT INTO app_position
VALUES (16, 'Position 3', 'Job position 3 descriptipn', 'Job position 3 responsibilities', 'Job position 3 skills',
        'Job position 3 requirements', '3', '3000.0-6000.0', 'Brisbane', 'Permanent', 4);
INSERT INTO app_position
VALUES (17, 'Position 0', 'Job position 0 descriptipn', 'Job position 0 responsibilities', 'Job position 0 skills',
        'Job position 0 requirements', '0', '0.0-0.0', 'Brisbane', 'Permanent', 5);
INSERT INTO app_position
VALUES (18, 'Position 1', 'Job position 1 descriptipn', 'Job position 1 responsibilities', 'Job position 1 skills',
        'Job position 1 requirements', '1', '1000.0-2000.0', 'Brisbane', 'Permanent', 5);
INSERT INTO app_position
VALUES (19, 'Position 2', 'Job position 2 descriptipn', 'Job position 2 responsibilities', 'Job position 2 skills',
        'Job position 2 requirements', '2', '2000.0-4000.0', 'Brisbane', 'Permanent', 5);
INSERT INTO app_position
VALUES (20, 'Position 3', 'Job position 3 descriptipn', 'Job position 3 responsibilities', 'Job position 3 skills',
        'Job position 3 requirements', '3', '3000.0-6000.0', 'Brisbane', 'Permanent', 5);
INSERT INTO app_position
VALUES (21, 'Position 0', 'Job position 0 descriptipn', 'Job position 0 responsibilities', 'Job position 0 skills',
        'Job position 0 requirements', '0', '0.0-0.0', 'Brisbane', 'Permanent', 6);
INSERT INTO app_position
VALUES (22, 'Position 1', 'Job position 1 descriptipn', 'Job position 1 responsibilities', 'Job position 1 skills',
        'Job position 1 requirements', '1', '1000.0-2000.0', 'Brisbane', 'Permanent', 6);
INSERT INTO app_position
VALUES (23, 'Position 2', 'Job position 2 descriptipn', 'Job position 2 responsibilities', 'Job position 2 skills',
        'Job position 2 requirements', '2', '2000.0-4000.0', 'Brisbane', 'Permanent', 6);
INSERT INTO app_position
VALUES (24, 'Position 3', 'Job position 3 descriptipn', 'Job position 3 responsibilities', 'Job position 3 skills',
        'Job position 3 requirements', '3', '3000.0-6000.0', 'Brisbane', 'Permanent', 6);
CREATE TABLE IF NOT EXISTS "app_jobseeker"
(
    "id"             integer      NOT NULL PRIMARY KEY AUTOINCREMENT,
    "skills"         varchar(511) NULL,
    "visa_status"    varchar(63)  NULL,
    "qualifications" varchar(512) NULL,
    "resume"         varchar(127) NULL,
    "user_id"        integer      NOT NULL REFERENCES "app_user" ("id") DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO app_jobseeker
VALUES (1, 'Python, PHP, C++0', 'Student Visa0', 'Ozzy Diploma0', './resumes/resume0.pdf', 1);
INSERT INTO app_jobseeker
VALUES (2, 'Python, PHP, C++1', 'Student Visa1', 'Ozzy Diploma1', './resumes/resume1.pdf', 2);
INSERT INTO app_jobseeker
VALUES (3, 'Python, PHP, C++2', 'Student Visa2', 'Ozzy Diploma2', './resumes/resume2.pdf', 3);
INSERT INTO app_jobseeker
VALUES (4, 'Python, PHP, C++3', 'Student Visa3', 'Ozzy Diploma3', './resumes/resume3.pdf', 4);
INSERT INTO app_jobseeker
VALUES (5, 'Python, PHP, C++4', 'Student Visa4', 'Ozzy Diploma4', './resumes/resume4.pdf', 5);
CREATE TABLE IF NOT EXISTS "app_employment"
(
    "id"            integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "started"       date    NOT NULL,
    "finished"      date    NOT NULL,
    "job_seeker_id" integer NOT NULL REFERENCES "app_jobseeker" ("id") DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO app_employment
VALUES (1, '2015-02-01', '2020-04-01', 1);
INSERT INTO app_employment
VALUES (2, '2016-02-01', '2021-04-01', 1);
INSERT INTO app_employment
VALUES (3, '2017-02-01', '2022-04-01', 1);
INSERT INTO app_employment
VALUES (4, '2018-02-01', '2023-04-01', 1);
INSERT INTO app_employment
VALUES (5, '2015-02-01', '2020-04-01', 2);
INSERT INTO app_employment
VALUES (6, '2016-02-01', '2021-04-01', 2);
INSERT INTO app_employment
VALUES (7, '2017-02-01', '2022-04-01', 2);
INSERT INTO app_employment
VALUES (8, '2018-02-01', '2023-04-01', 2);
INSERT INTO app_employment
VALUES (9, '2015-02-01', '2020-04-01', 3);
INSERT INTO app_employment
VALUES (10, '2016-02-01', '2021-04-01', 3);
INSERT INTO app_employment
VALUES (11, '2017-02-01', '2022-04-01', 3);
INSERT INTO app_employment
VALUES (12, '2018-02-01', '2023-04-01', 3);
INSERT INTO app_employment
VALUES (13, '2015-02-01', '2020-04-01', 4);
INSERT INTO app_employment
VALUES (14, '2016-02-01', '2021-04-01', 4);
INSERT INTO app_employment
VALUES (15, '2017-02-01', '2022-04-01', 4);
INSERT INTO app_employment
VALUES (16, '2018-02-01', '2023-04-01', 4);
INSERT INTO app_employment
VALUES (17, '2015-02-01', '2020-04-01', 5);
INSERT INTO app_employment
VALUES (18, '2016-02-01', '2021-04-01', 5);
INSERT INTO app_employment
VALUES (19, '2017-02-01', '2022-04-01', 5);
INSERT INTO app_employment
VALUES (20, '2018-02-01', '2023-04-01', 5);
CREATE TABLE IF NOT EXISTS "app_employer"
(
    "id"      integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "user_id" integer NOT NULL REFERENCES "app_user" ("id") DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO app_employer
VALUES (1, 6);
INSERT INTO app_employer
VALUES (2, 7);
INSERT INTO app_employer
VALUES (3, 8);
INSERT INTO app_employer
VALUES (4, 9);
INSERT INTO app_employer
VALUES (5, 10);
INSERT INTO app_employer
VALUES (6, 11);
CREATE TABLE IF NOT EXISTS "app_application"
(
    "id"               integer      NOT NULL PRIMARY KEY AUTOINCREMENT,
    "personal_details" varchar(255) NOT NULL,
    "contact_details"  varchar(255) NOT NULL,
    "date"             date         NOT NULL,
    "job_seeker_id"    integer      NOT NULL REFERENCES "app_jobseeker" ("id") DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO app_application
VALUES (1, 'Personal details 0 test0', 'Ozzy street 120, OzzyState 00 OzzyTown Australia', '2020-02-01', 1);
INSERT INTO app_application
VALUES (2, 'Personal details 0 test1', 'Ozzy street 120, OzzyState 01 OzzyTown Australia', '2020-02-01', 1);
INSERT INTO app_application
VALUES (3, 'Personal details 0 test2', 'Ozzy street 120, OzzyState 02 OzzyTown Australia', '2020-02-01', 1);
INSERT INTO app_application
VALUES (4, 'Personal details 1 test0', 'Ozzy street 121, OzzyState 10 OzzyTown Australia', '2020-02-01', 2);
INSERT INTO app_application
VALUES (5, 'Personal details 1 test1', 'Ozzy street 121, OzzyState 11 OzzyTown Australia', '2020-02-01', 2);
INSERT INTO app_application
VALUES (6, 'Personal details 1 test2', 'Ozzy street 121, OzzyState 12 OzzyTown Australia', '2020-02-01', 2);
INSERT INTO app_application
VALUES (7, 'Personal details 2 test0', 'Ozzy street 122, OzzyState 20 OzzyTown Australia', '2020-02-01', 3);
INSERT INTO app_application
VALUES (8, 'Personal details 2 test1', 'Ozzy street 122, OzzyState 21 OzzyTown Australia', '2020-02-01', 3);
INSERT INTO app_application
VALUES (9, 'Personal details 2 test2', 'Ozzy street 122, OzzyState 22 OzzyTown Australia', '2020-02-01', 3);
INSERT INTO app_application
VALUES (10, 'Personal details 3 test0', 'Ozzy street 123, OzzyState 30 OzzyTown Australia', '2020-02-01', 4);
INSERT INTO app_application
VALUES (11, 'Personal details 3 test1', 'Ozzy street 123, OzzyState 31 OzzyTown Australia', '2020-02-01', 4);
INSERT INTO app_application
VALUES (12, 'Personal details 3 test2', 'Ozzy street 123, OzzyState 32 OzzyTown Australia', '2020-02-01', 4);
INSERT INTO app_application
VALUES (13, 'Personal details 4 test0', 'Ozzy street 124, OzzyState 40 OzzyTown Australia', '2020-02-01', 5);
INSERT INTO app_application
VALUES (14, 'Personal details 4 test1', 'Ozzy street 124, OzzyState 41 OzzyTown Australia', '2020-02-01', 5);
INSERT INTO app_application
VALUES (15, 'Personal details 4 test2', 'Ozzy street 124, OzzyState 42 OzzyTown Australia', '2020-02-01', 5);

from django.db import models


class User(models.Model):
    first_name = models.CharField(max_length=63, null=False, blank=False)
    middle_names = models.CharField(max_length=127, null=True, blank=False)
    last_name = models.CharField(max_length=63, null=False, blank=False)
    username = models.CharField(max_length=63, null=False, blank=False, unique=True)
    password = models.CharField(max_length=63, null=False, blank=False)


class Employment(models.Model):
    job_seeker = models.ForeignKey("JobSeeker", on_delete=models.CASCADE)
    started = models.DateField(null=False)
    finished = models.DateField(null=False)


class JobSeeker(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    visa_status = models.CharField(max_length=63, null=True)
    resume = models.CharField(max_length=127, null=True)


class JobSeekerSkill(models.Model):
    job_seeker = models.ForeignKey(JobSeeker, on_delete=models.CASCADE)
    name = models.CharField(max_length=127, null=False, blank=False)
    years_of_experience = models.IntegerField(null=False)


class Employer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Position(models.Model):
    title = models.CharField(max_length=63, null=False, blank=False)
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    description = models.CharField(max_length=255, null=False, blank=True)
    responsibilities = models.CharField(max_length=511, null=False, blank=True)
    requirements = models.CharField(max_length=511, null=False, blank=True)
    years_of_experience = models.IntegerField(null=False)
    salary_min = models.FloatField(null=True)
    salary_max = models.FloatField(null=True)
    location = models.CharField(max_length=63, null=False, blank=False)
    contract_type = models.CharField(max_length=63, null=True)


class PositionSkill(models.Model):
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    name = models.CharField(max_length=127, null=False, blank=False)
    years_of_experience = models.IntegerField(null=False)


class Application(models.Model):
    personal_details = models.CharField(max_length=255, null=False, blank=True)
    contact_details = models.CharField(max_length=255, null=False, blank=True)
    date = models.DateField(null=False)
    job_seeker = models.ForeignKey(JobSeeker, on_delete=models.CASCADE)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)

from random import randrange

from port.models import Application, User, Employment, Employer, JobSeeker, Position, PositionSkill, JobSeekerSkill
from datetime import datetime

# from port.tests import create_test_data

position_names = [
    "Software Developer",
    "Database Developer",
    "Data Scientist"
]

skills = [
    "Software Testing",
    "Java",
    "Python"
]


def create_test_data():
    for en in range(6):
        user = User(fist_name=f"OzzyEmployer{en}",
                    middle_names=f"OzzyEmployerMidName{en}",
                    last_name=f"OzzyEmployerLastName{en}",
                    username=f"OzzyEmployer{en}",
                    password=f"dfkb3aanbi4gv{en}")
        user.save()
        employer = Employer(
            user=user
        )
        employer.save()

        for pn in range(4):
            position = Position(
                title=position_names[randrange(0, len(position_names))],
                employer=employer,
                description=f"Job position {pn} descriptipn",
                responsibilities=f"Job position {pn} responsibilities",
                skills=f"Job position {pn} skills",
                requirements=f"Job position {pn} requirements",
                years_of_experience=f"{pn}",
                salary_min=pn * 1e3,
                salary_max=pn * 2e3,
                location=f"Brisbane",
                contract_type=f"Permanent",
            )
            position.save()

            for pn in range(5):
                skill = PositionSkill(
                    position=position,
                    name=skills[randrange(0, len(skills))],
                    years_of_experience=randrange(1, 8)
                )
                skill.save()

    positions = Position.objects.all()
    for un in range(5):
        user = User(fist_name=f"OzzyName{un}",
                    middle_names=f"OzzyMidName{un}",
                    last_name=f"OzzyLastName{un}",
                    username=f"OzzyUsername{un}",
                    password=f"dfkb3aanbi4gv{un}")
        user.save()

        job_seeker = JobSeeker(user=user,
                               skills=f"Python, PHP, C++{un}",
                               visa_status=f"Student Visa{un}",
                               qualifications=f"Ozzy Diploma{un}",
                               resume=f"./resumes/resume{un}.pdf")
        job_seeker.save()

        for an in range(3):
            app = Application(
                personal_details=f"Personal details {un} test{an}",
                contact_details=f"Ozzy street 12{un}, OzzyState {un}{an} OzzyTown Australia",
                date=datetime(2020, 2, 1),
                position=positions[randrange(0, len(positions))],
                job_seeker=job_seeker
            )
            app.save()

        for en in range(4):
            employment = Employment(
                job_seeker=job_seeker,
                started=datetime(2015 + en, 2, 1),
                finished=datetime(2020 + en, 4, 1),
            )
            employment.save()

        for en in range(4):
            skill = JobSeekerSkill(
                job_seeker=job_seeker,
                name=skills[randrange(0, len(skills))],
                years_of_experience=randrange(1, 8)
            )
            skill.save()


def test_query_1():
    employers = Employer.objects.filter()


def test_query_2():
    pass


def test_query_3():
    pass


def test_query_4():
    pass


def test_query_5():
    pass


def test_query_6():
    pass
